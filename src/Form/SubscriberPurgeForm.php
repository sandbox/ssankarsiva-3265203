<?php

namespace Drupal\purge_ach_import_queue\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;

/**
 * {@inheritdoc}
 */
class SubscriberPurgeForm extends FormBase {

  /**
   * Subscriber Import Queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'subscriberpurgeform';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $queue_factory = \Drupal::service('queue');
    $queue = $queue_factory->get('acquia_contenthub_subscriber_import');
    $queue_count = $queue->numberOfItems();
    $form['description'] = [
      '#markup' => $this->t('Instruct the content hub module to manage content import queue.'),
    ];

    if ($queue_count > 0) {
      $form['run_import_queue'] = [
        '#type' => 'details',
        '#title' => $this->t('Clear Import Queue'),
        '#open' => TRUE,
      ];
      $form['run_import_queue']['purge'] = [
        '#type' => 'submit',
        '#value' => $this->t('Purge'),
        '#name' => 'purge_import_queue',
        '#disabled' => FALSE,
        '#attributes' => [
          'onclick' => 'if(!confirm("Are you sure?")){return false;}',
        ],
      ];
    }
    else {
      $form['run_import_queue'] = [
        '#type' => 'details',
        '#title' => $this->t('Clear Import Queue'),
        '#description' => $this->t('Import queue is empty'),
        '#open' => TRUE,
      ];
      $form['run_import_queue']['purge'] = [
        '#type' => 'submit',
        '#value' => $this->t('Purge'),
        '#name' => 'purge_import_queue',
        '#disabled' => TRUE,
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $queue_factory = \Drupal::service('queue');
    $queue = $queue_factory->get('acquia_contenthub_subscriber_import');
    $queue->deleteQueue();
    $this->messenger()->addMessage($this->t('Purged all contenthub import queues.'));
  }

}
